<?php
/** 
 * @package Aggiunta IVA a CSV per TrovaPrezzi
 * @author Leo Pizzolante <leo@pizzolante.biz>
 * @date 17/10/2017
 * #rev1
 */
if (isset($_FILES['elaborare']['tmp_name'])) {
  $text_file = $_FILES['elaborare']['tmp_name'];
  $iva = (double) $_POST['iva'];
}
$result =  csvToArray($text_file, $iva);


$stream = fopen('data://text/plain,' . "", 'w+');
foreach ($result as $res) {
  fputcsv($stream, $res,"|");

}
rewind($stream);

header('Content-Type: application/csv');
header('Content-Disposition: attachement; filename="file.csv";');
fpassthru($stream);

fclose($stream);


function csvToArray($in_file, $iva =  1.22) {
  $arr_Return = array();
  $file = fopen($in_file, 'r');
  while (($line = fgetcsv($file, 0, "|")) !== FALSE) {
    $spedizione = number_format(floatval(str_replace(',', '.', $line[7])), 2);
    $spedizione_ivata = number_format(floatval( (float) $spedizione*  (float) $iva), 2);
    if (is_array($line)) {
      $line[7] = str_replace('.', ',', $spedizione_ivata);
      $arr_Return[] = $line;
    }
  }
  fclose($file);
  return $arr_Return;
}

?>